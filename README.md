# gitlab-storage-cleanup

Gitlab recently started enforcing [comically small storage quotas](https://news.ycombinator.com/item?id=32386323) on projects and namespaces. They don't, however, have a good way to cleanup old CI artifacts from projects at a namespace level. This project iterates through projects and deletes old artifacts based on the age threshold you set.

## Usage

Checkout and install dependencies:

```bash
git clone git@gitlab.com:thelabnyc/gitlab-storage-cleanup.git
cd gitlab-storage-cleanup
poetry install
```

Show help:

```bash
poetry run gitlab-storage-cleanup --help
```

Delete all artifacts older than 14 days for the project `my-group/myproject`.

```bash
GITLAB_ACCESS_TOKEN='foo-bar-baz' poetry run gitlab-storage-cleanup '^my-group\/myproject$'
```

Delete all artifacts older than 30 days for all projects in under the `my-group` namespace.

```bash
GITLAB_ACCESS_TOKEN='foo-bar-baz' poetry run gitlab-storage-cleanup --min-days-old 30 '^my-group\/'
```

## Disclaimer

This script can't read your mind—it may delete artifacts you want or need. Audit the code before use and use it at your own risk. It doesn't currently have a dry-run mode, but if you feel like adding that, merge requests are welcome.
